'use strict';

let gulp = require('gulp');

gulp.task('jshint', () => {
	let jshint = require('gulp-jshint');

	const PATH_SRC = './public/js/**/*.js';

	return gulp.src(PATH_SRC)
		.pipe(jshint())
		.pipe(jshint.reporter('default'));
});

gulp.task('jade2html', () => {
	let jade = require('gulp-jade');

	const PATH_SRC = './app/public/views/**/*.jade';
	const PATH_DEST = './public';

	return gulp.src(PATH_SRC)
		.pipe(jade())
		.pipe(gulp.dest(PATH_DEST));
});

gulp.task('stylus2css', () => {
	let stylus = require('gulp-stylus');

	const PATH_SRC = './app/public/style/**/*.styl';
	const PATH_DEST = './public/css';

	return gulp.src(PATH_SRC)
			.pipe(stylus())
			.pipe(gulp.dest(PATH_DEST));
});

gulp.task('build-angular-app', ['jshint'], () => {
	let uglify = require('gulp-uglify'),
			concat = require('gulp-concat');

	const PATH_SRC = './public/js/**/*.js';
	const PATH_SRC_IGNORE = '!./public/js/app.min.js';
	const PATH_DEST = './public/js';

	return gulp.src([PATH_SRC, PATH_SRC_IGNORE])
			.pipe(uglify())
			.pipe(concat('app.min.js'))
			.pipe(gulp.dest(PATH_DEST));
});

gulp.task('default', ['build-angular-app','jade2html','stylus2css']);