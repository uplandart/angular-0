'use strict';

/* Filters */

var angularAppFilters = angular.module('angularAppFilters', []);

angularAppFilters.filter('checkmark', function() {
	return function(input) {
		return input ? '\u2713' : '\u2718';
	};
});