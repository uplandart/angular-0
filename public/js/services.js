'use strict';

/* Services */

var angularAppServices = angular.module('angularAppServices', ['ngResource']);

angularAppServices.factory('Phone', [
		'$resource',
		function($resource) {
			return $resource('phones/:phoneId.json', {}, {
				query: {method:'GET', params:{phoneId:'phones'}, isArray:true }
			});
		}
]);